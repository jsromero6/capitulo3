#Reescribe el programa del salario usando try y except, de modo que el programa 
#sea capaz de gestionar entradas no numéricas con elegancia, mostrando
#un mensaje y saliendo del programa.

try:
    numHoras = float(input('Ingrese el número de horas trabajadas: '))
    tarifaHora = float(input('Ingrese la tarifa por hora: '))
    salario = numHoras * tarifaHora
    if numHoras > 40:
        horasExtra = numHoras - 40
        print('El número de horas extras trabajadas es: ', horasExtra)
        salario = (numHoras - horasExtra) * tarifaHora
        print('El salario base es: ', salario)
        tarifaExtra = (tarifaHora * 1.5) * horasExtra
        print('El valor por horas extra es: ', tarifaExtra)
        salarioExtra = tarifaExtra + salario
        print('El salario total es: ', salarioExtra)
    elif numHoras <= 40:
        print('El salario total es: ', salario)
except:
    print('Por favor, ingrese un tipo de dato numérico')