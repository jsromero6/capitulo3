#Escribe un programa que solicite una puntuación entre 0.0 y 1.0. Si la
#puntuación está fuera de ese rango, muestra un mensaje de error. Si la puntuación
#está entre 0.0 y 1.0, muestra la calificación usando la tabla siguiente:

#Puntuación Calificación 
# >= 0.9 Sobresaliente 
# >= 0.8 Notable 
# >= 0.7 Bien 
# >= 0.6 Suficiente
# < 0.6 Insuficiente
try:
    punt = float(input('Ingrese una puntuación entre 0.0 y 1.0: '))
    if punt>=0.9 and punt<=1.0:
        print('Sobresaliente')
    elif punt>=0.8:
        print('Notable')
    elif punt>=0.7:
        print('Bien')
    elif punt>=0.6:
        print('Suficiente')
    elif punt < 0.6:
        print('Insuficiente')
    else:
        print('La puntuación ingresada está fuera del rango permitido')
except:
    print('Por favor, ingrese solo datos numéricos')