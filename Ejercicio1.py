#Reescribe el programa del cálculo del salario para darle al empleado 1.5 veces la tarifa horaria para todas las horas trabajadas que excedan de 40.

numHoras = float(input('Ingrese el número de horas trabajadas: '))
tarifaHora = float(input('Ingrese la tarifa por hora: '))
salario = numHoras * tarifaHora

if numHoras > 40:
    horasExtra = numHoras - 40
    print('El número de horas extras trabajadas es: ', horasExtra)
    salario = (numHoras - horasExtra) * tarifaHora
    print('El salario base es: ', salario)
    tarifaExtra = (tarifaHora * 1.5) * horasExtra
    print('El valor por horas extras es: ', tarifaExtra)
    salarioExtra = tarifaExtra + salario
    print('El salario total es: ', salarioExtra)
elif numHoras <= 40:
    print('El salario total es: ', salario)